#!/bin/bash

items=(1 "Express+TS"
       2 "Fastify+TS"
       3 "NestJS+TS"
)

while choice=$(dialog --title "$TITLE" \
                 --menu "Please select" 10 40 3 "${items[@]}" \
                 2>&1 >/dev/tty)
    do
    case $choice in
        1) exec ./express/start.sh;;
        2) exec ./fastify/start.sh;;
        3) exec ./nestjs/start.sh;;
        *) ;;
    esac
done
clear # clear after user pressed Cancel
