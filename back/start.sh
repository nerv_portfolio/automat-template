#!/bin/bash

items=(1 "Express"
       2 "Fastify"
       3 "NestJS"
)

while choice=$(dialog --title "$TITLE" \
                 --menu "Please select" 10 40 3 "${items[@]}" \
                 2>&1 >/dev/tty)
    do
    case $choice in
        1) exec ./back/express/start.sh;;
        2) exec ./fastify/start.sh;;
        3) exec ./nestjs/start.sh;;
        *) ;;
    esac
done
clear # clear after user pressed Cancel
