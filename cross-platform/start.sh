#!/bin/bash

items=(1 "Electron"
       )

while choice=$(dialog --title "$TITLE" \
                 --menu "Please select" 10 40 3 "${items[@]}" \
                 2>&1 >/dev/tty)
    do
    case $choice in
        1) exec ./electron/start.sh;; # some action on 1
        *) ;; # some action on other
    esac
done
clear # clear after user pressed Cancel
