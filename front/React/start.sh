#!/bin/bash

items=(1 "Angular"
       2 "React"
       2 "Vue")

while choice=$(dialog --title "$TITLE" \
                 --menu "Please select" 10 40 3 "${items[@]}" \
                 2>&1 >/dev/tty)
    do
    case $choice in
        1) exec ./Angular/starts.sh;;
        2) exec ./React/starts.sh;;
        3) exec ./Vue/starts.sh;;
        *) ;;
    esac
done
clear # clear after user pressed Cancel
