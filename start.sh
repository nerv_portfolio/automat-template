#!/bin/bash

items=(1 "Front"
       2 "Back"
       3 "Cross platform"
       4 "Browser extension"
       5 "Make package"
)

while choice=$(dialog --title "$TITLE" \
                 --menu "Please select" 10 40 3 "${items[@]}" \
                 2>&1 >/dev/tty)
    do
    case $choice in
        1) exec ./front/start.sh;; # some action on 1
        2) exec ./back/start.sh;; # some action on 2
        3) exec ./cross-platform/start.sh;; # some action on 2
        4) exec ./browser-ext/start.sh;; # some action on 2
        *) ;; # some action on other
    esac
done
clear # clear after user pressed Cancel
